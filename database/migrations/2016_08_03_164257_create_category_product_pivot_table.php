<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

if(!class_exists('CreateCategoryProductPivotTable')){

    class CreateCategoryProductPivotTable extends Migration
    {
        /**
        * Run the migrations.
        *
        * @return void
        */
        public function up()
        {
            Schema::create('category_product', function (Blueprint $table) {
                $table->integer('category_id')->unsigned()->index();
                $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
                $table->integer('product_id')->unsigned()->index();
                $table->primary(['category_id', 'product_id']);
            });
        }

        /**
        * Reverse the migrations.
        *
        * @return void
        */
        public function down()
        {
            Schema::dropIfExists('category_product');
        }
    }
}
