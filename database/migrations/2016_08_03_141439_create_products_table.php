<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

if(!class_exists('CreateProductsTable')){

    class CreateProductsTable extends Migration
    {
        /**
        * Run the migrations.
        *
        * @return void
        */
        public function up()
        {
            Schema::create('products', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();
                $table->string('name');
                $table->string('slug');
                $table->string('loc');
                $table->string('lastmod');
                $table->string('image');
                $table->string('location');
                $table->text('description');
                $table->integer('price');
                $table->integer('price_reduced');
                $table->string('condition');
                $table->integer('marketplace_id');
            });
        }

        /**
        * Reverse the migrations.
        *
        * @return void
        */
        public function down()
        {
            Schema::dropIfExists('products');
        }
    }
}
