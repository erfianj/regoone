<?php
return [
    'name' => 'Regoone!',
    'search' => [
        'limit' => 100,
    ],
    'product' => [
        'relations' => [
            'cacheable' => [
                'categories' => function($q){

                    $q->remember(44640);
                },
                'marketplace' => function($q){

                    $q->remember(44640);
                },
                'likeCounter'
            ],
        ],
    ],
    // scraper settings
    'tokopedia' => [
        'seed' => [
            'name' => 'Tokopedia',
            'slug' => 'tokopedia.com',
            'mascot' => 'https://ecs7.tokopedia.net/img/microsite-brand-resource/mascot-toped-new.png',
            'logo' => 'http://i.imgur.com/vxDgccV.png',
        ],
        'sitemap_url' => 'https://s3-ap-southeast-1.amazonaws.com/tokopedia-upload/sitemap/products-index.xml'
    ],
    'bukalapak' => [
        'seed' => [
            'name' => 'Bukalapak',
            'slug' => 'bukalapak.com',
            'mascot' => 'https://www.bukalapak.com/images/logo-google-graph.png',
            'logo' => 'https://s1.inkuiri.net/a/files/images/logo-detail-re00-bukalapak.png'
        ],
        'sitemap_url' => 'https://www.bukalapak.com/sitemap-categories.xml',
    ],

    'cache_duration' => 86400,
    'cdn' => 'http://i1.wp.com/',
    'sitemap' => [
        'products_per_sitemap' => 25000
    ],
    'term_size' => 1000,
    'translations' => [
        'location' => 'lokasi',
        'marketplace:slug' => 'di',
        'categories:slug' => '',
        '(le)' => 'maksimal ',
        '(ge)' => 'minimal ',
        '(lt)' => 'kurang dari',
        '(gt)' => 'lebih dari',
        'sort' => '',
        'price,asc' => 'harga termurah',
        'price,desc' => 'harga termahal',
        'updated_at,asc' => 'update terlama',
        'updated_at,desc' => 'update terbaru',
        'query' => '',
        'view_as' => 'tampilkan sebagai',
        'list' => 'daftar',
        'thumbnail' => 'kotak',
        '%%' => 'semua',
        'condition' => 'kondisi',
        'new' => 'baru',
        'used' => 'bekas',
        'price' => 'harga',
        '%' => '',
        'debug' => ''
    ],
    'like' => [
        'label' => 'Favorit',
    ],
    'crawler' => [
        'skip_if_exists' => true,
    ],
    'worker' => [
        'max_queue_size' => 3000000,
    ]
];

