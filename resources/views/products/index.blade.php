@extends('layouts.app')

@section('content')
	<div class="row">
		
		<div class="col-md-9">
			<div class="row">
				<div class="col-md-12">
					
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-primary"> 
						        <div class="panel-heading"> 
									{!! Breadcrumbs::render('search', $query, $marketplace) !!}
						            <div class="pull-right">
						            	View as: &nbsp;
						            	<div class="btn-group">
						            		<a rel="nofollow" href="{{ build_filter(['view_as' => 'list']) }}" class="white"><i class="glyphicon glyphicon-th-list" aria-hidden="true"></i> List</a>

						            		&nbsp;

							            	<a href="{{ build_filter(['view_as' => 'thumbnail']) }}" class="white"><i class="glyphicon glyphicon-th" aria-hidden="true"></i> Thumbnail</a>
						            	</div>

						            </div>
						        </div> 
						        
						    </div>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h1 id="product-title">{!! highlight(ucwords($title), $q) !!} </h1>
							<p>Mendapatkan lebih dari {{ $total }} produk untuk pencarian tersebut</p>
							<hr>
						</div>
					</div>
					<div class="infinite-scroll">
						@widget('products', [
							'title' => '',
							'products' => $products,
							'item_class' => 'col-md-3',
							'chunk_class' => 'row',
							'chunk' => 4,
							'view_as' => request()->input('view_as', 'list'),
							'q' => $q,
						])
					</div>
					
					<div class="text-center" id="pagination">
						{{ $products->appends($query)->links() }}
					</div>
				</div>
			</div>
		</div>

		

		<div class="col-md-3">
			@if($marketplace)
			<div class="panel panel-default text-center">
				<img class="img-responsive" src="{{ $marketplace->mascot }}" alt="{{ $marketplace->name }} mascot">
				{{ $marketplace->slug }}
			</div>
			@widget('categoryFilter', ['marketplace' => $marketplace])

			@endif
				<!-- todo:	productSorter widget -->
			@widget('priceChooser', ['ids' => $ids, 'total' => $total])
			@widget('timeChooser', ['ids' => $ids, 'total' => $total])
			@widget('locationFilter', ['ids' => $ids, 'total' => $total])
			@widget('categoryChooser', ['ids' => $ids, 'total' => $total])
			@widget('marketplaceFilter', ['ids' => $ids, 'total' => $total])
			
			@widget('productFilter')

		</div>
	</div>
@endsection

@section('script')
$(document).ready(function(){
	$('.dropdown-submenu a.test').on("click", function(e){
		$(this).next('ul').toggle();
    	e.stopPropagation();
	    e.preventDefault();
	});

	$('.infinite-scroll').infinitescroll({
 
	    navSelector  : "div#pagination",            
	                   // selector for the paged navigation (it will be hidden)
	    nextSelector : 'ul.pager li a[rel="next"]',    
	                   // selector for the NEXT link (to page 2)
	    itemSelector : "div.products"          
	                   // selector for all items you'll retrieve
	});
});
@endsection