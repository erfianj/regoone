@extends('layouts.app')

@section('content')
    @widget('intro')
    @widget('marketplaces')
    @widget('bannerCards')
    @widget('products', [
        'title' => 'Terbaru',
        'item_class' => 'col-md-2',
        'products' => App\Product::remember(5)->limit(6)->with([
        'marketplace' => function($q){
            $q->remember(5);
        }, 
        'categories' => function($q){
            $q->remember(5);
        }
        ])->orderBy('id', 'desc')->get(),
    ])


@endsection

@section('script')
    $(".share-icons").jsSocials({
        showLabel: false,
        showCount: false,
        shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
    });
@endsection
