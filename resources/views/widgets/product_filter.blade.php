<div class="panel panel-default" style="background: #f7f7f7; border-top:3px solid #337ab7; padding: 10px;">
	<form action="{{ route('product.index') }}">
		@if(request()->has('query'))
			<input type="hidden" name="query" value="{{ request()->input('query') }}">
		@endif
		<!-- sort -->
		<div class="form-group">
			<label>Urutkan</label>
			<select name="sort" class="form-control">
				<option @if(request()->input('sort') === 'relevance') selected="selected" @endif value="relevance">Paling Relevan</option>
				<option @if(request()->input('sort') === 'price,asc') selected="selected" @endif value="price,asc">Harga Termurah</option>
				<option @if(request()->input('sort') === 'price,desc') selected="selected" @endif value="price,desc">Harga Termahal</option>
				<option @if(request()->input('sort') === 'updated_at,desc') selected="selected" @endif value="updated_at,desc">Update Terbaru</option>
				<option @if(request()->input('sort') === 'updated_at,asc') selected="selected" @endif value="updated_at,asc">Update Terlama</option>
			
			</select>
		</div>


		<!-- marketplace -->
		<div class="form-group">
			<label>Marketplace</label>
			<select name="marketplace:slug" class="form-control">
				<option @if(request()->input('marketplace:slug') === '%%') selected="selected" @endif value="%%">Semua Marketplace</option>
				@foreach($marketplaces as $marketplace)
				<option @if(request()->input('marketplace:slug') === $marketplace->slug) selected="selected" @endif value="{{ $marketplace->slug }}">{{ $marketplace->name }}</option>
				@endforeach
			</select>
		</div>

		<!-- harga -->
		<div class="form-group">
			<label>Harga</label>
			<div class="form-group">
				<div class="input-group">
				    <div class="input-group-addon">Rp</div>
				    <input name="price[min]" type="text" class="form-control" placeholder="Min" value="{{ (int)request()->input('price.min') ?: ''  }}">
			    </div>
			</div>
			<div class="form-group">
				<div class="input-group">
				    <div class="input-group-addon">Rp</div>
				    <input name="price[max]" type="text" class="form-control" placeholder="Max" value="{{ (int)request()->input('price.max') ?: ''  }}">
			    </div>
			</div>
		</div>

		<!-- kondisi -->
		<div class="form-group">
			<label>Kondisi</label>
			<select name="condition" class="form-control">
				<option value="%%">Baru &amp; Bekas</option>
				<option value="baru">Baru</option>
				<option value="bekas">Bekas</option>
			</select>
		</div>

		<!-- lokasi -->
		<div class="form-group">
			<label>Lokasi</label>
			<select name="location" class="form-control">
				<option @if(request()->input('location') === '%%') selected="selected" @endif value="%%">Semua Lokasi</option>
				@foreach($locations as $location)
				<option @if(request()->input('location') === $location) selected="selected" @endif value="{{ strtolower($location) }}">{{ ucwords($location) }}</option>
				@endforeach
			</select>
		</div>

		<button class="btn btn-primary btn-block" type="submit"><i class="fa fa-filter" aria-hidden="true"></i> Filter</button>

	</form>
	<a href="{{ url(request()->path()) }}" class="btn btn-block btn-link"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
 Reset Filter</a>
</div>