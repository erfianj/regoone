<div class="list-group">
	<div href="#" class="list-group-item active">
    	<span class="panel-title">
			<i class="glyphicon glyphicon-tags"></i> Pilih Kategori
		</span>
  	</div>
	@foreach($categories as $category)
	<a href="{{ build_filter(['categories:slug' => $category->slug]) }}" class="list-group-item">
		<span class="badge">
			@if($ratio <= 1)
	    	{{ rp_terbilang($category->total) }}
	    	@else
	    	{{ rp_terbilang($category->total*$ratio) }}
	    	@endif
		</span>
    {{ $category->name }}
    </a>    
    @endforeach
</div>