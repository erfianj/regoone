<div class="list-group">
	<div class="list-group-item active">
    	<span class="panel-title">
			<i class="glyphicon glyphicon-calendar"></i> Pilih Waktu
		</span>
  	</div>
	@foreach($times as $time)
    <a href="{{ build_filter(['created_at' => '(ge)' . $time->date_filter]) }}" class="list-group-item">
    <span class="badge">
    	@if($ratio <= 1)
    	{{ rp_terbilang($time->total) }}
    	@else
    	{{ rp_terbilang($time->total*$ratio) }}
    	@endif
    </span>
    {{ $time->date_interval }}</a>
    @endforeach
</div>