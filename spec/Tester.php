<?php
class Tester
{
    use Illuminate\Foundation\Testing\Concerns\MakesHttpRequests;

    protected $baseUrl = 'http://lapak.app';
    protected $app;

    public function __construct()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        $this->app = $app;
    }

    public function crawler() {
        return $this->crawler;
    }

    public function response()
    {
        return $this->response;
    }
}
