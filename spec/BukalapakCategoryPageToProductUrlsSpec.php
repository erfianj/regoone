<?php
use App\Marketplace;
use App\Jobs\BukalapakCategoryPageToProductUrls;

describe('BukalapakCategoryPageToProductUrls', function(){
    putenv('APP_ENV=testing');

    $this->scraper = new BukalapakCategoryPageToProductUrls('https://www.bukalapak.com/c/komputer?from=category_home&search[keywords]=&page=3');

    describe('->handle()', function(){

        it('should enqueue bukalapak:product_url_to_product', function(){
            $this->scraper->handle();
            $this->jobs = Redis::llen('queues:bukalapak:product_url_to_product');
            expect($this->jobs)->not->toBeEmpty();
        });
    });

    after(function(){
        Artisan::call('migrate:refresh', ['--force' => true]);
        Redis::flushAll();
    });
});
