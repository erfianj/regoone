<?php
use Carbon\Carbon;
use App\Marketplace;
use App\Category;
use App\Product;
use App\Jobs\TokopediaProductsWorker;

describe('TokopediaProductUrlToProduct', function(){

    $this->product = [
        'loc' => 'https://www.tokopedia.com/ibnuminatimerch/boardshort-ripcurl-original-cps-ripcurl-384',
        'lastmod' => new Carbon('2016-07-18T08:46:50+08:00')
    ];

    $this->processor = new App\Jobs\TokopediaProductUrlToProduct($this->product);
    describe('->handle()', function(){

        it('should create or select tokopedia marketplace', function(){
            $this->processor->handle();
            $toped = Marketplace::where('slug', '=', 'tokopedia.com')->first();
            expect($toped)->not->toBeNull();
        });

        it('should create relevant categories', function(){
            $product = Product::where('loc', '=', $this->product['loc'])->with('categories')->first();


            expect($product->categories()->count())->not->toBe(0);
        });

    });

    after(function(){
        Artisan::call('migrate:refresh', ['--force' => true]);
        Redis::flushAll();
    });
});
