<?php
use App\Console\Commands\BukalapakScraper;
use App\Marketplace;

describe('BukalapakScraper', function(){

    $this->scraper = new BukalapakScraper;

    describe('->handle()', function(){
        it('should create or select bukalapak marketplace', function(){
            $this->scraper->handle();
            $marketplace = Marketplace::where('slug', '=', 'bukalapak.com')->first();

            expect($marketplace)->not->toBeNull();
        });

        it('should enqueue bukalapak:sitemap_url_to_categories', function(){
            $this->jobs = Redis::llen('queues:bukalapak:sitemap_url_to_categories');
            expect($this->jobs)->not->toBeEmpty();
        });
    });

    after(function(){
        Artisan::call('migrate:refresh', ['--force' => true]);
        Redis::flushAll();
    });
});
