<?php
use App\Marketplace;
use App\Jobs\BukalapakSitemapUrlToCategories;

describe('BukalapakSitemapUrlToCategories', function(){

    $this->scraper = new BukalapakSitemapUrlToCategories('https://www.bukalapak.com/sitemap-categories.xml');

    describe('->handle()', function(){

        it('should enqueue bukalapak:category_to_category_pages', function(){
            $this->scraper->handle();
            $this->jobs = Redis::llen('queues:bukalapak:category_to_category_pages');
            expect($this->jobs)->not->toBeEmpty();
        });
    });

    after(function(){
        Artisan::call('migrate:refresh', ['--force' => true]);
        Redis::flushAll();
    });
});
