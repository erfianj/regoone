<?php

describe('@widget("products")', function(){
    $this->output = Widget::run('products')->toHtml();
    it('should not produce empty output', function(){
        expect($this->output)->not->toBeEmpty();
    });
});
