<?php

describe('@widget("marketplaces")', function(){
    $this->output = Widget::run('marketplaces')->toHtml();
    it('should not produce empty output', function(){
        expect($this->output)->not->toBeEmpty();
    });
});
