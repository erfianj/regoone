<?php
use App\Console\Commands\TokopediaScraper;

describe('TokopediaScraper', function(){

    $this->scraper = new TokopediaScraper;
    $this->tester = new Tester;

    describe('->handle()', function(){
        it('should enqueue tokopedia:sitemap_url_to_product_sitemaps', function(){
            $this->scraper->handle();

            $this->jobs_count = Redis::llen('queues:tokopedia:sitemap_url_to_product_sitemaps');
            expect($this->jobs_count)->not->toBeEmpty();
        });
    });

    after(function(){
        Artisan::call('migrate:refresh', ['--force' => true]);
        Redis::flushAll();
    });
});
