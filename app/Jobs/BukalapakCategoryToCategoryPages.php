<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Symfony\Component\DomCrawler\Crawler;


class BukalapakCategoryToCategoryPages extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $category_url;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($category_url)
    {
        $this->category_url = $category_url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->enqueueCategoryPages();
    }

    public function enqueueCategoryPages()
    {
        $crawler = new Crawler(@file_get_contents($this->category_url));

        $last_page = (count($crawler->filter('span.last-page')) > 0) ? $crawler->filter('span.last-page')->text() : 1;

        if($last_page > 4 && env('APP_ENV') == 'testing'){
            $last_page = 4;
        }

        for ($i=1; $i <= $last_page; $i++) {
            $this->enqueueCategoryPage($i);
        }

        free_memory($i);
        free_memory($crawler);
        free_memory($last_page);
    }

    public function enqueueCategoryPage($index)
    {
        $category_page_url = $this->category_url . '&page=' . $index;

        $job = (new BukalapakCategoryPageToProductUrls($category_page_url))
        ->onQueue('bukalapak:category_page_to_product_urls');
        dispatch($job);

        free_memory($index);
        free_memory($category_page_url);
        free_memory($job);
    }
}
