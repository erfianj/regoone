<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Marketplace;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Symfony\Component\DomCrawler\Crawler;
use SoapBox\Formatter\Formatter;
use Cache;


class TokopediaProductSitemapToProductUrls extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $sitemap;
    protected $products;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sitemap)
    {
        $this->sitemap = $sitemap;
        $this->products = [];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(isOverload('tokopedia:product_url_to_product')){
            return ;
        }

        $this->queueProducts();
    }

    public function queueProducts()
    {
        $this->getProducts();

        foreach ($this->products as $key => $product) {
            dispatch((new TokopediaProductUrlToProduct($product))->onQueue('tokopedia:product_url_to_product'));
        }
    }

    public function getProducts()
    {
        $this->products = Cache::remember('sitemap_products_' . $this->sitemap['loc'], config('site.cache_duration'), function(){
            $sitemap = $this->decodeSitemap();

            $crawler = new Crawler;
            $crawler->addHtmlContent($sitemap);

            $nodes = $crawler->filter('url');

            $products = [];
            foreach ($nodes as $key => $node) {
                if($key == 4 && env('APP_ENV') === 'testing'){
                    break;
                }

                $node = new Crawler($node);
                $product = [];
                $product['loc'] = $node->filter('loc')->text();
                $product['lastmod'] = new Carbon($node->filter('lastmod')->text());

                $products[] = $product;

                // free up memory
                free_memory($product);
                free_memory($node);
                free_memory($key);

            }

            free_memory($data);
            free_memory($sitemap);
            free_memory($crawler);

            return $products;
        });
    }

    public function decodeSitemap()
    {
        return Cache::remember('sitemap_' . $this->sitemap['loc'], config('site.cache_duration'), function(){

            $url = "compress.zlib://" . $this->sitemap['loc'];
            return @file_get_contents($url);
        });
    }
}
