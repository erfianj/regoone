<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Symfony\Component\DomCrawler\Crawler;


class BukalapakSitemapUrlToCategories extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $sitemap_url;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sitemap_url)
    {
        $this->sitemap_url = $sitemap_url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->enqueueCategories();
    }

    public function enqueueCategories()
    {
        $categories = $this->getCategories($this->sitemap_url);

        foreach ($categories as $category) {
            $this->enqueueCategory($category);
        }
    }

    public function enqueueCategory($category)
    {
        $job = (new BukalapakCategoryToCategoryPages($category))->onQueue('bukalapak:category_to_category_pages');
        dispatch($job);
    }

    /**
     * return $categories: list of category url ready to scrape pagination at
     */
    public function getCategories($sitemap_url)
    {
        $sitemap_content = @file_get_contents($sitemap_url);
        $crawler = new Crawler();
        $crawler->addHtmlContent($sitemap_content);

        $nodes = $crawler->filter('url loc');

        foreach ($nodes as $key => $node) {
            $node = new Crawler($node);
            $category =  trim($node->text() . '?from=category_home&search[keywords]=');

            $categories[] = $category;

            // free up memory
            free_memory($key);
            free_memory($node);
            free_memory($category);
        }

        // free up memory
        free_memory($sitemap_content);
        free_memory($crawler);
        free_memory($nodes);

        return $categories;
    }
}
