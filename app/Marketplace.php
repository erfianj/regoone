<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;

class Marketplace extends Model
{
    use Rememberable;
    
    protected $fillable = ['name', 'slug', 'mascot', 'logo'];

    public function categories()
    {
        return $this->hasMany('App\Category');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function getLogoAttribute()
    {
    	return url('images/marketplaces/colors/' . str_slug($this->name) . '.png');
    }
}
