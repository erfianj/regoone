<?php

// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', url(''));
});

// Home
Breadcrumbs::register('user_product_favorite', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Produk Favorit', url('dashboard'));
});

Breadcrumbs::register('marketplace', function($breadcrumbs, $marketplace)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push($marketplace->name, route('marketplace.show', ['slug' => $marketplace->slug]));
});

Breadcrumbs::register('category', function($breadcrumbs, $categories){

	if(is_array($categories) || $categories instanceof Kalnoy\Nestedset\Collection){
		$cat = (isset($categories[0])) ? $categories[0] : $categories->first();

		$current_path = route('marketplace.show', ['slug' => $cat->marketplace->slug]);
		foreach ($categories as $key => $category) {
			if($key == 0) $breadcrumbs->parent('marketplace', $category->marketplace);
			$breadcrumbs->push($category->name, build_category_url($category->slug, $current_path));
			$current_path .= '/' . $category->slug;
		}
	}
	else{
		$category = $categories;
		$breadcrumbs->parent('marketplace', $category->marketplace);
		$breadcrumbs->push($category->name, build_category_url($category->slug));
	}
});

Breadcrumbs::register('product', function($breadcrumbs, $product){
	$breadcrumbs->parent('category', $product
		->categories()
		->remember(10080)
		->with([
			'marketplace' => function($q){
				$q->remember(10080);
			}
		])
		->get());
	$breadcrumbs->push($product->name, build_product_url_for($product));
});

Breadcrumbs::register('search', function($breadcrumbs, $query, $marketplace = null){
{
	$breadcrumbs->parent('home');


	$breadcrumbs->push('Pencarian', route('product.index', $query));

}
});