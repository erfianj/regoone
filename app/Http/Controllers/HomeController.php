<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Product;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $products = Product::getLikes($user->id)
        ->with('marketplace', 'categories')
        ->with('likeCounter')
         // highly suggested to allow eager load
        ->whereLikedBy($user->id) // find only articles where user liked them
        ->simplePaginate(20);

        $title = 'Produk Favorit';

        return view('home', [
            'products' => $products,
            'user' => $user,
            'title' => $title,
            ]);
    }
}
