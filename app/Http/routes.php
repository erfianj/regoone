<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

if(request()->input('debug', false)){
	\Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
	    var_dump($query->sql, $query->bindings, $query->time);
	});
}

$s = 'social.';
Route::get('/social/redirect/{provider}',   ['as' => $s . 'redirect',   'uses' => 'Auth\AuthController@getSocialRedirect']);
Route::get('/social/handle/{provider}',     ['as' => $s . 'handle',     'uses' => 'Auth\AuthController@getSocialHandle']);


Route::get('/', function () {
	SEOMeta::setTitle('Bandingkan Sebelum Belanja');
    return view('welcome');
});

Route::get('jump', ['as' => 'product.redirect', 'uses' => 'ProductController@redirect']);

// sitemap routes
Route::get('products', ['as' => 'product.index', 'uses' => 'ProductController@index']);
Route::post('products/{id}/like', ['as' => 'product.like', 'uses' => 'ProductController@like']);
Route::post('products/{id}/unlike', ['as' => 'product.unlike', 'uses' => 'ProductController@unlike']);
Route::get('sitemap.xml', ['as' => 'sitemap.index', 'uses' => 'SitemapController@index']);
Route::get('sitemap-general.xml', ['as' => 'sitemap.index', 'uses' => 'SitemapController@general']);
Route::get('sitemap-product.xml', ['as' => 'sitemap.product', 'uses' => 'SitemapController@product']);


Route::get('sites/{slug}', ['as' => 'marketplace.show', 'uses' => 'MarketplaceController@show']);
Route::get('/sites/{marketplace}/{hierarchy}', ['as' => 'category.show', 'uses' => 'CategoryController@show'])
->where('hierarchy', '(.*)?');


Route::auth();

Route::get('/dashboard', 'HomeController@index');
