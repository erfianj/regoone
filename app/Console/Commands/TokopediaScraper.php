<?php

namespace App\Console\Commands;

use App\Jobs\TokopediaSitemapUrlToProductSitemaps;
use Cache;
use Illuminate\Config\Repository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;
use Redis;

class TokopediaScraper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:tokopedia';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape Tokopedia.com products and returns the products in JSON format, so the products can be easily imported';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->sitemap_url = config('site.tokopedia.sitemap_url');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(Redis::llen('queues:tokopedia:sitemap_url_to_product_sitemaps')){
            $this->info('Tokopedia scraper already running, please wait until it dispatched');
            return;
        }

        if(env('APP_ENV') !== 'testing')
            $this->info('Running tokopedia scraper');

        $job = (new TokopediaSitemapUrlToProductSitemaps($this->sitemap_url))->onQueue('tokopedia:sitemap_url_to_product_sitemaps');
        dispatch($job);
    }
}
