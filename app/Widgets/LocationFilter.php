<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use DB;
use App\Product;

class LocationFilter extends AbstractWidget
{
    public $cacheTime = 44640;
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $ids = $this->config['ids'];
        $total = $this->config['total'];
        $ratio = round($total/20);

        $locations = Product::whereIn('id', $ids)->select(DB::raw('count(*) as total'), 'location')
        ->groupBy('location')
        ->lists('total','location')->all();

        return view("widgets.location_filter", [
            'config' => $this->config,
            'locations' => $locations,
            'ratio' => $ratio
        ]);
    }
}