<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Category;

class CategoryFilter extends AbstractWidget
{
    // public $cacheTime = 44640;
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'category' => null,
        'parent' => null,
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $categories = Category::remember(10080)->where('marketplace_id', '=', $this->config['marketplace']->id)->whereNull('parent_id')->get();
        
        return view("widgets.category_filter", [
            'config' => $this->config,
            'categories' => $categories
        ]);
    }
}