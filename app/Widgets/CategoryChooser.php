<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Category;
use DB;

class CategoryChooser extends AbstractWidget
{
    public $cacheTime = 44640;
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $ids = $this->config['ids'];
        $total = $this->config['total'];
        $ratio = round($total/20);

        $categories = DB::table('category_product')
        ->select(DB::raw('count(category_id) as total, categories.slug, categories.name'))
        ->whereIn('product_id', $ids)
        ->join('categories', 'categories.id', '=', 'category_id')
        ->groupBy('categories.slug')
        ->get();

        return view("widgets.category_chooser", [
            'config' => $this->config,
            'categories' => $categories,
            'ratio' => $ratio
        ]);
    }
}