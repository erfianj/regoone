<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class Product extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $product = ($this->config['product']) ?: null;
        $related_products = ($this->config['related_products']) ?: null;

        return view("widgets.product", [
            'config' => $this->config,
            'product' => $product,
            'related_products' => $related_products
        ]);
    }
}