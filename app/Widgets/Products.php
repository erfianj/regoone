<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use DB;

class Products extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'title' => 'Terbaru',
        'chunk_class' => 'row',
        'item_class' => 'col-md-2',
        'products' => [],
        'chunk' => 6,
        'view_as' => 'thumbnail',
        'q' => '',
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {

    	if(empty($this->config['products'])){
    	    $this->config['products'] = collect([]);

    	}
        return view("widgets.products." . $this->config['view_as'], [
            'config' => $this->config,
            'q' => $this->config['q'],
        ]);
    }
}
