<?php
use App\Product;

function free_memory(&$var){
    // $memory_usage = memory_get_usage();

    $var = null;
    unset($var);

    // $memory_usage = round((memory_get_usage() - $memory_usage)/1024, 2);
    // echo "memory usage: " . $memory_usage . "KB\n";
    //
    // $memory_usage = null;
    // unset($memory_usage);
}

function isOverload($queueName)
{
    $queueName = 'queues:' . $queueName;
    $queueLength = Redis::llen($queueName);
    $maxSize = config('site.worker.max_queue_size');

    $isOverload = ($queueLength >= $maxSize) ? true : false;

    Log::info('Queue: ' . $queueName);
    Log::info('Length: ' . $queueLength);
    Log::info('Max Size: ' . $maxSize);
    Log::info('Overload: ' . (string)$isOverload);

    return $isOverload;
}

function format_money($value)
{
    return 'Rp. ' . number_format( $value, 0 , '' , '.' ) . ',-';
}

/**
 * build filter query string for filter widget
 * @param  array $additional_query array of additional query string to add to url
 * @return string 					url of new query
 */
function build_filter($additional_query, $current_input = null, $current_path = null)
{
	
	$current_input = ($current_input) ?: request()->input();
	$current_path = ($current_path) ?: request()->path();
	
	
	$query = array_merge($current_input, $additional_query)	;
	ksort($query);
	$query = http_build_query($query);


	return url($current_path) .  '?' . $query;
}

function build_category_url($slug, $current_path = null, $current_input = null)
{
	$current_path = ($current_path) ?: request()->path();
	$current_input = ($current_input) ?: request()->input();
	$current_input = http_build_query($current_input);

	$url = url($current_path . '/' . $slug);

	if(!empty($current_input)){
		$url .=  '?' . $current_input;
	}

	return $url;
}

function build_category_parent_url($current_path = null, $current_input = null)
{
	$current_path = ($current_path) ?: request()->path();
	$current_input = ($current_input) ?: request()->input();
	$current_path = explode('/', $current_path);

	array_pop($current_path);

	$current_path = implode('/', $current_path);
	$current_input = http_build_query($current_input);

	$url = url($current_path);

	if(!empty($current_input)){
		$url .=  '?' . $current_input;
	}

	return $url;
}

function build_product_url_for($id)
{
	$product = $id;

	if(is_numeric($id)){
		
		$product = Product::with('categories', 'marketplace')->where('id', '=', $id)->first();
	}

	$url = 'sites/' . $product->marketplace->slug . '/';

	foreach ($product->categories as $category) {
		$url .= $category->slug . '/';
	}

	$url .= $product->id . '-' . $product->slug;

	return url($url);
}

function descendants($category)
{
	$categories[] = $category;
	while($category->parent_id != null){
		
		$category = $category->parent;
		array_unshift($categories, $category);
	}

	return $categories;
}

function save_or_get_term($term = '', $url = '')
{
	$terms = Cache::get('terms', collect([]));

    if(empty($term) or empty($url) or (str_contains($term, 'debug'))){
        return $terms;
    }

    $query = [
        'term' => $term,
        'url' => $url,
    ];

	$terms->prepend($query);

	$terms = $terms->unique();

	$terms = $terms->slice(0, config('site.term_size'));

	Cache::forever('terms', $terms);

	return $terms;
}

function get_term($terms = null)
{
    $terms = ($terms) ?: Cache::get('terms', collect([]));

    if($terms->first() !== null){
        return $terms->first();
    }
}

function build_title_from_array($array)
{
	$title = [];

    if(isset($array['query'])){
        $q = $array['query'];
        unset($array['query']);

        $array = ['query' => $q] + $array;
    }
    else{
        $title[] = 'barang';
    }

	$title_translations = config('site.translations');
	foreach ($array as $key => $value) {
		if($key == 'price'){

            if(is_string($value)){
                
    			$pieces = explode(')', $value);

    			$value = $pieces[0] . ')' . rp_terbilang($pieces[1]);
        		$title[] = strtolower($key . ' ' . $value);

            }

            if(is_array($value)){

                $temp_price = 'sekitar ';

                foreach ($value as $price) {
                    $pieces = explode(')', $price);

                    $price = rp_terbilang($pieces[1]);
                    $temp_price .= $price . ' ';
                }

                $title[] = $temp_price;
            }
        }

        elseif($key == 'condition' && $value == '%%'){
            $title[] = 'kondisi baru & bekas';
        }

        elseif($key == 'location' && $value == '%%'){
            $title[] = 'lokasi Indonesia';
        }

        else{

            $title[] = $key . ' ' . $value;
        }
	}

	$title = join(' ', $title);

	foreach ($title_translations as $key => $value) {
		$title = str_replace($key, $value, $title);
	}

	$title = 'harga ' . $title;
    $title = preg_replace('/\s+/', ' ',$title);

    $title = str_replace('lokasi semua', '', $title);
    $title = preg_replace('/\s+/', ' ',$title);

    return $title;
}

function rp_terbilang($angka, $debug = 0)
{
    
	$terbilang = '';
    $angka = str_replace(".",",",$angka);
    
    // list ($angka, $desimal) = explode(",",$angka);
    $panjang=strlen($angka);
    for ($b=0;$b<$panjang;$b++){
        $myindex=$panjang-$b-1;
        $a_bil[$b]=substr($angka,$myindex,1);
    }
    if ($panjang>9){
        $bil=$a_bil[9];
        if ($panjang>10){
            $bil=$a_bil[10].$bil;
        }

        if ($panjang>11){
            $bil=$a_bil[11].$bil;
        }
        if ($bil!="" && $bil!="000"){
            // return  rp_satuan($bil,$debug)." milyar";
            return  $bil . 'milyar';
        }
        
    }

    if ($panjang>6){
        $bil=$a_bil[6];
        if ($panjang>7){
            $bil=$a_bil[7].$bil;
        }

        if ($panjang>8){
            $bil=$a_bil[8].$bil;
        }
        if ($bil!="" && $bil!="000"){
            // return  rp_satuan($bil,$debug)." juta";
            return  $bil . 'juta';
        }
        
    }
    
    if ($panjang>3){
        $bil=$a_bil[3];
        if ($panjang>4){
            $bil=$a_bil[4].$bil;
        }

        if ($panjang>5){
            $bil=$a_bil[5].$bil;
        }
        if ($bil!="" && $bil!="000"){
            // return  rp_satuan($bil,$debug)." ribu";
            return  $bil . 'ribu';
        }
        
    }

    $bil=$a_bil[0];
    if ($panjang>1){
        $bil=$a_bil[1].$bil;
    }

    if ($panjang>2){
        $bil=$a_bil[2].$bil;
    }
    //die($bil);
    if ($bil!="" && $bil!="000"){
        // return  rp_satuan($bil,$debug);
        return  $bil;
    }
    return trim($terbilang);
}

function rp_satuan($angka,$debug = 0)
{
    $a_str['1']="satu";
    $a_str['2']="dua";
    $a_str['3']="tiga";
    $a_str['4']="empat";
    $a_str['5']="lima";
    $a_str['6']="enam";
    $a_str['7']="tujuh";
    $a_str['8']="delapan";
    $a_str['9']="sembilan";
    $terbilang = '';
    
    
    $panjang=strlen($angka);
    for ($b=0;$b<$panjang;$b++)
    {
        $a_bil[$b]=substr($angka,$panjang-$b-1,1);
    }
    
    if ($panjang>2)
    {
        if ($a_bil[2]=="1")
        {
            $terbilang=" seratus";
        }
        else if ($a_bil[2]!="0")
        {
            $terbilang= " ".$a_str[$a_bil[2]]. " ratus";
        }
    }

    if ($panjang>1)
    {
        if ($a_bil[1]=="1")
        {
            if ($a_bil[0]=="0")
            {
                return " sepuluh";
            }
            else if ($a_bil[0]=="1")
            {
                return " sebelas";
            }
            else 
            {
                return " ".$a_str[$a_bil[0]]." belas";
            }
            return $terbilang;
        }
        else if ($a_bil[1]!="0")
        {
            return " ".$a_str[$a_bil[1]]." puluh";
        }
    }
    
    if ($a_bil[0]!="0")
    {
        return " ".$a_str[$a_bil[0]];
    }
    return $terbilang;
   
}

function getLogos()
{
    $logos = [];
    foreach (glob(public_path('images/marketplaces') . '/*.png') as $logo) {
        $logos[] = str_replace(public_path(), '', $logo);
    }

    return $logos;
}

/**
 * Highlighting matching string
 * @param   string  $text           subject
 * @param   string  $words          search string
 * @return  string  highlighted text
 */
function highlight($text, $words) {
    if(!empty($words)){
        preg_match_all('~\w+~', $words, $m);
        if(!$m)
            return $text;
        $re = '~\\b(' . implode('|', $m[0]) . ')\\b~i';
        return preg_replace($re, '<b>$0</b>', $text);
    }

    return $text;
}

function base64url_encode($data) { 
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
} 
function base64url_decode($data) { 
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
} 

function get_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
    $url = 'https://www.gravatar.com/avatar/';
    $url .= md5( strtolower( trim( $email ) ) );
    $url .= "?s=$s&d=$d&r=$r";
    if ( $img ) {
        $url = '<img src="' . $url . '"';
        foreach ( $atts as $key => $val )
            $url .= ' ' . $key . '="' . $val . '"';
        $url .= ' />';
    }
    return $url;
}