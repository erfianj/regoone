<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Product;

class ProductWidgetTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testOutput()
    {
        $product = Product::with('categories')->find(1);
    
        $output = Widget::run('product', ['product' => $product, 'related_products' => $product->related()])->toHtml();

        $this->assertNotEmpty($output);
    }
}
