<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BreadcrumbTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoaded()
    {
        $this->assertTrue(class_exists('Breadcrumbs'));
    }

    public function testHome()
    {
    	$bc = Breadcrumbs::render('home');

    	$this->assertContains('Home', $bc);
    }

    public function testMarketplace()
    {
    	$marketplace = App\Marketplace::where('slug', '=', 'tokopedia.com')->first();
    	$bc = Breadcrumbs::render('marketplace', $marketplace);

    	$this->assertContains('Tokopedia', $bc);
    }

    public function testCategory()
    {
    	$marketplace = App\Marketplace::where('slug', '=', 'tokopedia.com')->first();
    	$category = $marketplace->categories()->withDepth()->having('depth', '=', 0)->first();

    	// dd($category->toArray());

    	$bc = Breadcrumbs::render('category', $category);

    	$this->assertContains(e($category->name), $bc);
    }

    public function testNestedCategories()
    {
    	$marketplace = App\Marketplace::where('slug', '=', 'tokopedia.com')->first();
    	$category = $marketplace->categories()->withDepth()->having('depth', '=', 0)->first();
    	$children = $category->children()->first();

    	$bc = Breadcrumbs::render('category', [$category, $children]);

    	$this->assertContains(e($category->name), $bc);
    }


    public function testSearch()
    {
        $q = 'designing web';
        $marketplace = App\Marketplace::where('slug', '=', 'bukalapak.com')->first();
        $bc = Breadcrumbs::render('search', ['q' => $q], $marketplace);
        $this->assertContains('Pencarian', $bc);
    }

    public function testProdukFavorit()
    {
        $bc = Breadcrumbs::render('user_product_favorite');

        $this->assertContains('Favorit', $bc);
    }
}
