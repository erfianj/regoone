<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HelperTest extends TestCase
{
	public function testBuildTitleFromArray()
	{
		$array = [
			'query' => 'Tools Set Sepeda',
			'categories:slug' => 'otomotif',
			'marketplace:slug' => 'tokopedia',
			'price' => '(ge)20000',
		];

		$title = build_title_from_array($array);

		$this->assertEquals('harga Tools Set Sepeda otomotif di tokopedia harga minimal 20ribu', $title);
	}
}
