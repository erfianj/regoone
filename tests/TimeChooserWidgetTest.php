<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Product;

class TimeChooserWidgetTest extends TestCase
{
	public function testOutput()
	{
		$query = Product::search('putih');
    	$output = Widget::run('timeChooser', [
    		'ids' => $query->limit(100)->pluck('id'),
	    	'total' => $query->count()])
    	->toHtml();

    	$this->assertNotEmpty($output);
	}
}
