<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Product;

class RelatedProductsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testMethod()
    {
        $products = Product::search('kumpulan koleksi dvd')->limit(3)->get();

        $this->assertNotNull($products);
    }

    public function testRelated()
    {
        $product_url = build_product_url_for(1);

        $this->visit($product_url)
        ->see('Produk Sejenis');
    }
}
