<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SeoTest extends TestCase
{
    public function testHome()
    {
    	$this->visit('')
    	->seeInElement('title', config('site.name'));
    }

    public function testMarketplace()
    {
    	$this->visit('sites/bukalapak.com')
    	->seeInElement('title', 'Bukalapak');
    }

    public function testCategory()
    {
    	$this->visit('sites/bukalapak.com/komputer')
    	->seeInElement('title', 'Kategori Komputer di Bukalapak');
    }

    public function testProduct()
    {
        $product = App\Product::find(1);
    	$this->visit(build_product_url_for($product->id))
    	->seeInElement('title', $product->name);


    }

    public function testSearch()
    {
    	$this->visit('products?query=asus+zenfone')
    	->seeInElement('title', 'Asus Zenfone');
    }
}
