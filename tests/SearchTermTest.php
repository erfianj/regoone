<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SearchTermTest extends TestCase
{
    public function testSaveTerm()
    {
    	$this->assertTrue(function_exists('save_or_get_term'), 'function save_or_get_term should be defined');

    	Redis::del('term');

    	$new_term = 'makan nasi pakai telor';
        $url = route('product.index', ['q' => $new_term]);

    	$terms = save_or_get_term($new_term, $url);

    	$this->assertTrue($terms->count() == 1, 'should save term in redis');

    }
}
