<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Product;

class LocationFilterWidgetTest extends TestCase
{
    public function testOutput()
    {
    	$query = Product::search('putih');
    	$output = Widget::run('locationFilter', [
    		'ids' => $query->limit(100)->pluck('id'),
	    	'total' => $query->count()])
    	->toHtml();

    	$this->assertNotEmpty($output);
    }
}
