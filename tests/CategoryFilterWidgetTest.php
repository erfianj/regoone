<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Marketplace;

class CategoryFilterWidgetTest extends TestCase
{
    public function testOutput()
    {
    	$marketplace = Marketplace::first();
    	$categories = $marketplace->categories()->get()->toTree();
    	$output = Widget::run('categoryFilter', ['marketplace' => $marketplace, 'categories' => $categories]);

    	$this->assertNotEmpty($output);
    }
}
