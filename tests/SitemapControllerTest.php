<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use SebastianBergmann\Diff\Differ;


class SitemapControllerTest extends TestCase
{
    public function testSitemapIndex()
    {
    	$this->visit('sitemap.xml')
    	->assertResponseOk()
    	->see('sitemap');
    }

    public function testSitemapGeneral()
    {
    	Redis::del('terms');
        $this->visit('sitemap-general.xml')
    	->assertResponseOk();

        save_or_get_term('makan nasi', url('some_url'));
        $output = $this->visit('sitemap-general.xml')
        ->see(url('some_url'));
    }

}
